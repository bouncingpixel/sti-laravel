PHP-Laravel Docker images
=================

This repository contains the source for building various versions of
the PHP with Laravel application as a reproducible Docker image using
[source-to-image](https://github.com/openshift/source-to-image).
Users can choose between RHEL and CentOS based builder images.
The resulting image can be run using [Docker](http://docker.io).

For more information about using these images with OpenShift, please see the
official [OpenShift Documentation](https://docs.openshift.org/latest/using_images/s2i_images/php.html).

Versions
---------------
Laravel and PHP versions currently supported are:
* 5.2 with php-5.6

Node versions used for Laravel-Elixir:
* 4.4

RHEL versions currently supported are:

CentOS versions currently supported are:
* CentOS7


Installation
---------------
To build a Laravel image, choose either the CentOS or RHEL based image:
*  **RHEL based image** *not supported at this time*

    To build a RHEL based Laravel-5.2 image, you need to run the build on a properly
    subscribed RHEL machine.

    ```
    $ git clone https://github.com/BouncingPixel/sti-laravel.git
    $ cd sti-laravel
    $ make build TARGET=rhel7 VERSION=5.2
    ```

*  **CentOS based image**
    ```
    $ git clone https://github.com/BouncingPixel/sti-laravel.git
    $ cd sti-laravel
    $ make build VERSION=5.2
    ```

Alternatively, you can pull the CentOS image from Docker Hub via:

    $ docker pull bouncingpixel/laravel-52-centos7

**Notice: By omitting the `VERSION` parameter, the build/test action will be performed
on all the supported versions of Laravel and PHP.**


Usage
---------------------------------

For information about usage of Dockerfile for Laravel 5.2 and PHP 5.6,
see [usage documentation](5.2/README.md).

Test
---------------------
This repository also provides a [S2I](https://github.com/openshift/source-to-image) test framework,
which launches tests to check functionality of a simple PHP application built on top of the sti-laravel image.

Users can choose between testing a PHP test application based on a RHEL or CentOS image.

*  **RHEL based image** *not supported at this time*

    This image is not available as a trusted build in [Docker Index](https://index.docker.io).

    To test a RHEL7 based Laravel-5.2 image, you need to run the test on a properly
    subscribed RHEL machine.

    ```
    $ cd sti-laravel
    $ make test TARGET=rhel7 VERSION=5.2
    ```

*  **CentOS based image**

    ```
    $ cd sti-laravel
    $ make test VERSION=5.2
    ```

**Notice: By omitting the `VERSION` parameter, the build/test action will be performed
on all the supported versions of Laravel. Since we currently only support version `5.2`
you can omit this parameter.**


Repository organization
------------------------
* **`<php-version>`**

    Dockerfile and scripts to build container images from.

* **`hack/`**

    Folder containing scripts which are responsible for the build and test actions performed by the `Makefile`.

Image name structure
------------------------
##### Structure: bouncingpixel/1-2-3

1. Platform name (lowercase) - laravel
2. Platform version(without dots) - 52
3. Base builder image - centos7

Examples: `bouncingpixel/laravel-52-centos7`
